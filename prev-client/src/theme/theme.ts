const theme = {
  colors: {
    background: '#FFFFFF',
    paper: '#FFFFFF',
    button: '#393D45',
    text: '#000',
    primary: '#F26526',
    textSecondary: 'white',
    errorMessage: '#800',
    successMessage: '#080',
    span: '#949595',
  },
};
export default theme;

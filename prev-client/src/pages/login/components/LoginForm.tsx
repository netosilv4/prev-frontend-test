import React, { useContext, useState } from 'react';
import { Input } from 'antd';
import {
  FormContainer, InputSpan, Label, LinkButton, LoginButton,
} from './styles';
import { Logo } from '../../../styles/Logo';
import { UserContext } from '../../../context/UserContext/UserContext';

const LoginForm = () => {
  const { handleLogin } = useContext(UserContext);
  const [user, setUser] = useState('');
  const [password, setPassword] = useState('');

  return (
    <FormContainer>
      <Logo width="100px" src="./icons/logo.svg" alt="logo" />
      <Label>
        <InputSpan>Email ou CPF</InputSpan>
        <Input value={user} onChange={({ target }) => setUser(target.value)} size="large" width={200} placeholder="Digite seu email ou CPF" />
      </Label>
      <Label>
        <InputSpan>Senha</InputSpan>
        <Input value={password} onChange={({ target }) => setPassword(target.value)} type="password" size="large" placeholder="Digite sua senha" />
      </Label>
      <LinkButton type="link">Esqueceu a senha?</LinkButton>
      <LoginButton onClick={() => handleLogin(user, password)} htmlType="submit" size="large">Entrar</LoginButton>
    </FormContainer>
  );
};
export default LoginForm;

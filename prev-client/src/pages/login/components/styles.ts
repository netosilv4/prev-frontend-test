import styled from 'styled-components';
import { Button, Form } from 'antd';

export const LoginButton = styled(Button)`
    background-color: ${(props) => props.theme.colors.primary};
    color: ${(props) => props.theme.colors.textSecondary};
    font-size: 16px;
    border-radius: 4px;
    border: none;
    :hover {
        background-color: rgba(242, 101, 38, 0.8);
        color: ${(props) => props.theme.colors.textSecondary};
    }
    &:focus {
        color: ${(props) => props.theme.colors.textSecondary};
        background-color: rgba(242, 101, 38, 0.8);
    }
`;

export const Label = styled.label`
    display: flex;
    flex-direction: column;
    font-size: 16px;
    width: 100%;
    gap: 5px;
`;

export const FormContainer = styled(Form)`
    width: 320px;
    flex-direction: column;
    display: flex;
    gap: 24px;
`;

export const InputSpan = styled.span`
    font-size: 13px;
    color: ${(props) => props.theme.colors.span};
`;

export const LinkButton = styled(Button)`
    align-self: flex-start;
    padding: 0;
    margin: 0;
    font-weight: bold;
    font-size: 14px;
`;

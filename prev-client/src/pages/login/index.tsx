import React from 'react';
import LoginForm from './components/LoginForm';
import { LoginContainer } from './styles';

const Login = () => (
  <LoginContainer>
    <LoginForm />
  </LoginContainer>
);

export default Login;

import { useState } from 'react';

const useUserModal = () => {
  const [isOpen, setIsOpen] = useState(false);

  return {
    isOpen,
    setIsOpen,
  };
};

export default useUserModal;

import React from 'react';
import DashboardProvider from '../../context/DashboardContext/DashboardContext';
import Navbar from '../../globalComponents/navbar';
import Petitions from './components/petitions';
import UserModules from './components/userModules';

const Dashboard = () => (
  <DashboardProvider>
    <Navbar />
    <UserModules />
    <Petitions />
  </DashboardProvider>
);

export default Dashboard;

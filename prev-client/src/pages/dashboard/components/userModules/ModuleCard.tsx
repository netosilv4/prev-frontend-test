import React from 'react';
import { ICounter } from '../../../../context/DashboardContext/useDashboard';
import { prettierIcon, prettierModule, prettierString } from '../../../../utils/prettiesModules';
import {
  ModuleCardContainer, ModuleTitle, ModuleTotal, ModuleTotalMonth, TitleWrapper,
} from './styles';

interface Props {
  module: ICounter;
}

const ModuleCard = (props: Props) => {
  const { module } = props;
  return (
    <ModuleCardContainer>
      <TitleWrapper>
        <img src={prettierIcon(module)} alt={module.tipo} />
        <ModuleTitle>{prettierString(module)}</ModuleTitle>
      </TitleWrapper>
      <ModuleTotal color={prettierModule(module)}>{module.total}</ModuleTotal>
      <ModuleTotalMonth>
        Este mês:
        {' '}
        {module.totalPeriodo.mensal }
      </ModuleTotalMonth>
    </ModuleCardContainer>
  );
};

export default ModuleCard;

import styled from 'styled-components';

export const ModuleCardContainer = styled.div`
    width: 170px;
    height: 130px;
    border-radius: 5px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.2);
    padding: 15px;
    @media (max-width: 768px) {
        width: 100%;
    }
`;

export const ModuleTitle = styled.span`
    font-size: 14px;
    font-weight: bold;
`;

export const ModuleTotal = styled.span`
    font-size: 34px;
    font-weight: bold;
    color: ${(props) => props.color};
`;

export const ModuleTotalMonth = styled.span`
    font-size: 14px;    
    color: #949595;
    font-weight: 400;
`;

export const Container = styled.div`
    width: 100%;
    display: flex;
    gap: 24px;
    align-items: center;
    justify-content: space-around;
    flex-wrap: wrap;
    margin-top: 50px;
`;

export const TitleWrapper = styled.div`
    display: flex;
    width: 100%;
    gap: 10px;
`;

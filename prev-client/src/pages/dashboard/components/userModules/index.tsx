import React, { useContext } from 'react';
import { DashboardContext } from '../../../../context/DashboardContext/DashboardContext';
import ModuleCard from './ModuleCard';
import { Container } from './styles';

const UserModules = () => {
  const { counters } = useContext(DashboardContext);

  return (
    <div>
      {counters ? (
        <Container>
          {counters.map((counter) => (
            <ModuleCard key={counter.tipo} module={counter} />
          ))}
        </Container>
      ) : (
        <Container>Carregando...</Container>
      )}
    </div>
  );
};

export default UserModules;

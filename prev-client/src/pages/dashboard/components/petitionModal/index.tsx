import { Descriptions } from 'antd';
import moment from 'moment';
import React from 'react';
import { IPetitions } from '../../../../context/DashboardContext/useDashboard';
import { StyledDescription } from '../../../../globalComponents/usermodal/styles';
import { ModalContainer } from './styles';

interface Props {
  petition: IPetitions | null;
  isOpen: boolean
  setIsOpen: (isOpen: boolean) => void
}

const PetitionModal = (props: Props) => {
  const { petition, isOpen, setIsOpen } = props;
  return (
    <ModalContainer
      visible={isOpen}
      onCancel={() => setIsOpen(false)}
      onOk={() => setIsOpen(false)}
    >
      <StyledDescription column={1} size="middle" title="Informações sobre a petição">
        <Descriptions.Item label="Nome">{ petition?.resumo }</Descriptions.Item>
        <Descriptions.Item label="Data de criação">{ moment(petition?.dataDeCriacao).format('DD/MM/YYYY - hh:mm:ss')}</Descriptions.Item>
        <Descriptions.Item label="Data da última atualizaçãio">{ moment(petition?.dataDaUltimaAtualizacao).format('DD/MM/YYYY - hh:mm:ss') }</Descriptions.Item>
        <Descriptions.Item label="Status">{ petition?.status }</Descriptions.Item>
      </StyledDescription>
    </ModalContainer>
  );
};

export default PetitionModal;

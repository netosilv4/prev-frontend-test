import Modal from 'antd/lib/modal/Modal';
import styled from 'styled-components';

export const ModalContainer = styled(Modal)`
    @media (max-width: 768px) {
        width: 100%;
        .ant-descriptions-item-container { 
            display: flex;
            flex-direction: column;
        }
    }
`;

import { Button, Pagination } from 'antd';
import styled from 'styled-components';

export const PetitionContainer = styled.section`
    padding: 50px 20px;
    display: flex;
    justify-content: space-between;
    width: 100%;
    flex-direction: column;
    gap: 24px;
    @media (max-width: 768px) {
        padding: 10px 10px;
    }
`;

export const PageTitle = styled.h1`
    margin-bottom: 50px;
    @media (max-width: 768px) {
        font-size: 16px;
        margin-top: 20px;
        margin-bottom: 0;
        font-weight: bold;
    }
`;

export const PetitionCardContainer = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    height: 200px;
    gap: 15px;
    box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.2);
    padding: 15px;
    @media (max-width: 768px) {
        height: auto;
        width: 100%;
    }
`;

export const PetitionTypes = styled.p`
    color: #3575AB;
    font-weight: bold;
    font-size: 14px;
`;

export const PetitionTypesContainer = styled.div`
    display: flex;
    width: 100%;
`;

export const PetitionStatus = styled.h1`
    font-size: 14px;
    font-weight: bold;
    color: ${(props) => props.theme.colors.primary};
`;

export const PetitionTitle = styled.p`
    font-size: 14px;
    font-weight: bold;
`;

export const LinkButton = styled(Button)`
    align-self: flex-start;
    padding: 0;
    margin: 0;
    font-weight: 300;
    font-size: 14px;
    color: #3575AB
    align-items: center;
    display: flex
    justify-content: center;
`;

export const PetitionLinks = styled.div`
    display: flex;
    justify-content: space-between;
    width: 100%;
    flex-wrap: wrap;
`;

export const PaginationContainer = styled(Pagination)`
    align-self: flex-end;
    @media (max-width: 768px) {
        align-self: center;
    }
`;

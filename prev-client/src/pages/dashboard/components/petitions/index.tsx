import React, { useContext, useEffect, useState } from 'react';
import { DashboardContext } from '../../../../context/DashboardContext/DashboardContext';
import { IPetitions } from '../../../../context/DashboardContext/useDashboard';
import useUserModal from '../../hooks/useUserModal';
import PetitionModal from '../petitionModal';
import PetitionCard from './petitionCard';
import { PageTitle, PaginationContainer, PetitionContainer } from './styles';

const Petitions = () => {
  const { petitions, petitionsMetadata, setPage } = useContext(DashboardContext);
  const { setIsOpen, isOpen } = useUserModal();
  const [selectedPetition, setSelectedPetition] = useState<IPetitions | null>(null);

  useEffect(() => {
    if (selectedPetition && !isOpen) setIsOpen(true);
  }, [selectedPetition]);

  useEffect(() => {
    if (!isOpen) setSelectedPetition(null);
  }, [isOpen]);

  return (
    <PetitionContainer>
      <PetitionModal setIsOpen={setIsOpen} isOpen={isOpen} petition={selectedPetition} />
      <PageTitle>Últimas petições</PageTitle>
      {
        petitions ? (
          <>
            {
            petitions.map((petition) => (
              <PetitionCard
                setSelectedPetition={setSelectedPetition}
                key={petition.id}
                petition={petition}
              />
            ))
            }
          </>
        ) : (
          <p>Nenhuma petição encontrada</p>
        )
          }
      <PaginationContainer
        pageSize={petitionsMetadata.limit}
        total={petitionsMetadata.totalPages}
        onChange={setPage}
        responsive
      />
    </PetitionContainer>
  );
};

export default Petitions;

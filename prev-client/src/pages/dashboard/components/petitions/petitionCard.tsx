import moment from 'moment';
import React from 'react';
import { IPetitions } from '../../../../context/DashboardContext/useDashboard';
import { Logo } from '../../../../styles/Logo';
import { prettierString } from '../../../../utils/prettierString';
import {
  LinkButton,
  PetitionCardContainer,
  PetitionLinks, PetitionStatus, PetitionTitle, PetitionTypes, PetitionTypesContainer,
} from './styles';

interface Props {
  petition: IPetitions
  setSelectedPetition: (petition: IPetitions) => void
}

const PetitionCard = (props: Props) => {
  const { petition, setSelectedPetition } = props;
  return (
    <PetitionCardContainer>
      <PetitionTypesContainer>
        {
        petition.tiposDeBeneficio.map((type) => (
          <PetitionTypes>
            {prettierString(type)}
          </PetitionTypes>
        ))
        }
      </PetitionTypesContainer>
      <PetitionStatus>{petition.status}</PetitionStatus>
      <PetitionTitle>{petition.titulo}</PetitionTitle>
      <PetitionLinks>
        <LinkButton type="link">
          Publicação:
          {' '}
          { moment(petition.dataDeCriacao).format('DD/MM/YYYY - hh:mm:ss') }
        </LinkButton>
        <LinkButton onClick={() => setSelectedPetition(petition)} type="link">
          <Logo width="20px" src="./icons/eye.svg" />
          Pré visualizar petição
        </LinkButton>
      </PetitionLinks>
    </PetitionCardContainer>
  );
};

export default PetitionCard;

interface LoginValidation {
  ok: boolean;
  message: string;
}

const validateLogin = (username: string, Password: string): LoginValidation => {
  switch (true) {
    case !username:
      return { ok: false, message: 'Por favor, insira um CPF ou Email válido.' };
    case !Password:
      return { ok: false, message: 'Senha não informada.' };
    case username.length < 3:
      return { ok: false, message: 'CPF ou email deve ter no mínimo 3 caracteres.' };
    case Password.length < 3:
      return { ok: false, message: 'Senha deve ter no mínimo 3 caracteres.' };
    default:
      return { ok: true, message: '' };
  }
};

export default validateLogin;

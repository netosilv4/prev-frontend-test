import axios from 'axios';
import { useState } from 'react';
import Swal from 'sweetalert2';
import decode from 'jwt-decode';
import validateLogin from '../../validations/login';

export interface Itoken {
  access_token: string;
  token_type: string;
}

export interface IUser {
  sub: string;
  conta: {
    ativo: boolean;
    email: string;
    modulos: string[];
    nome: string;
    privilegio: string
    urlImagemPerfil: string;
    uuid: string;
  }
}

const useUser = () => {
  const [token, setToken] = useState<Itoken | undefined>(undefined);
  const [user, setUser] = useState<IUser>({} as IUser);

  const decodeUser = (jwtToken: string) => {
    const decodedUser = decode(jwtToken) as IUser;
    return decodedUser;
  };

  const handleLogin = async (username: string, password: string) => {
    try {
      const { ok, message } = validateLogin(username, password);
      if (!ok) {
        return await Swal.fire({
          icon: 'error',
          title: 'Usuário ou senha inválidos',
          text: message,
        });
      }
      const { data } = await axios.post('http://localhost:8080/oauth', { username: 'teste', password: 'frontend' });
      localStorage.setItem('token', data.access_token);
      setToken(data);
      return setUser(decodeUser(data.access_token));
    } catch (err) {
      if (axios.isAxiosError(err)) {
        return Swal.fire('Ops', err.message, 'error');
      }
      return Swal.fire('Ops', 'Algo deu errado!', 'error');
    }
  };

  const handleLogout = () => {
    setToken(undefined);
  };

  return {
    token,
    setToken,
    handleLogin,
    handleLogout,
    user,
  };
};

export default useUser;

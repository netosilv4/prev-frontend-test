import React, { createContext } from 'react';
import useUser, { Itoken, IUser } from './useUser';

export const UserContext = createContext({} as IUserContext);

interface IUserContext {
  token: Itoken | undefined;
  user: IUser;
  handleLogin: (username: string, password: string) => void;
}

const UserProvider: React.FC = ({ children }) => {
  const { token, handleLogin, user } = useUser();
  return (
    <UserContext.Provider value={{ token, handleLogin, user }}>
      {children}
    </UserContext.Provider>
  );
};

export default UserProvider;

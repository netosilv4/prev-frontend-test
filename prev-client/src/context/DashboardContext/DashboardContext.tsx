import React, { createContext } from 'react';
import useDashboard, { ICounter, IPetitions, PetitionMetadata } from './useDashboard';

interface IDashboardCtx {
  counters: ICounter[] | null;
  petitions: IPetitions[] | null;
  petitionsMetadata: PetitionMetadata;
  setPage: (page: number) => void;
}

export const DashboardContext = createContext({} as IDashboardCtx);

const DashboardProvider: React.FC = ({ children }) => {
  const {
    counters, petitions, petitionsMetadata, setPage,
  } = useDashboard();
  return (
    <DashboardContext.Provider value={{
      counters, petitions, petitionsMetadata, setPage,
    }}
    >
      {children}
    </DashboardContext.Provider>
  );
};

export default DashboardProvider;

import {
  useCallback, useEffect, useState,
} from 'react';
import api from '../../services/api';

export interface ICounter {
  tipo: string
  total: number
  totalPeriodo: {
    mensal: number
  }
}

export interface PetitionMetadata {
  total: number
  page: number
  totalPages: number
  limit: number
}

export interface IPetitions {
  id: string,
  titulo: string,
  resumo: string,
  tipo: string,
  status: string,
  sexo: string,
  tipoDeProcesso: string,
  tipoDeAcao: string,
  tiposDeBeneficio: Array<string>,
  subtipo: Array<string>,
  competencia: string,
  carenciaMinima: number,
  slug: string,
  tags: Array<string>,
  criadoPor:string,
  atualizadoPor: string,
  curtidas: number,
  criticas: number,
  idadeMinima: number,
  ativa: boolean,
  score: number,
  profissoes: Array<string>,
  incapacidades: Array<string>,
  dataDeCriacao: string,
  dataDaUltimaAtualizacao: string
  periodo: string,
  highlights: Array<string>
}

const useDashboard = () => {
  const [counters, setCounter] = useState<ICounter[] | null>(null);
  const [petitions, setPetitions] = useState<IPetitions[] | null>(null);
  const [petitionsMetadata, setPetitionsMetadata] = useState<PetitionMetadata>({
    total: 0,
    page: 0,
    totalPages: 0,
    limit: 2,
  });

  const fetchCounters = useCallback(async () => {
    const { data } = await api.get('/counter');
    setCounter(data);
  }, []);

  const fetchPetitions = useCallback(async (page = 1, limit = 2) => {
    const { data, headers } = await api.get(`/peticoes?_page=${page}&_limit=${limit}`);
    setPetitions(data);
    setPetitionsMetadata({
      total: Number(headers['x-total-count']),
      page,
      totalPages: Number(headers['x-total-count']) / limit,
      limit: 2,
    });
  }, []);

  const setPage = (page: number) => {
    fetchPetitions(page, petitionsMetadata.limit);
  };

  useEffect(() => {
    fetchCounters();
    fetchPetitions();
  }, [fetchCounters]);

  return {
    counters,
    setCounter,
    petitions,
    petitionsMetadata,
    setPetitionsMetadata,
    setPage,
  };
};

export default useDashboard;

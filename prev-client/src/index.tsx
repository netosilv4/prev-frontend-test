import React from 'react';
import ReactDOM from 'react-dom';
import { ConfigProvider } from 'antd';
import ptBR from 'antd/lib/locale/pt_BR';
import { ThemeProvider } from 'styled-components';
import App from './App';
import theme from './theme/theme';
import GlobalStyle from './theme/global';
import 'antd/dist/antd.css';
import UserProvider from './context/UserContext/UserContext';

ReactDOM.render(
  <React.StrictMode>
    <ConfigProvider locale={ptBR}>
      <ThemeProvider theme={theme}>
        <UserProvider>
          <App />
        </UserProvider>
        <GlobalStyle />
      </ThemeProvider>
    </ConfigProvider>
  </React.StrictMode>,
  document.getElementById('root'),
);

import styled from 'styled-components';
import { Descriptions } from 'antd';

export const StyledDescription = styled(Descriptions)`
    .ant-descriptions-item-label {
        font-size: 14px;
        font-weight: bold;
    }
    .ant-descriptions-title {
        font-weight: bold;
        color: ${(props) => props.theme.colors.primary};
    }
`;

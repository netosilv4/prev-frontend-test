import { Descriptions } from 'antd';
import Modal from 'antd/lib/modal/Modal';
import React, { useContext } from 'react';
import { UserContext } from '../../context/UserContext/UserContext';
import { StyledDescription } from './styles';

interface Props {
  isOpen: boolean;
  setIsOpen: (isOpen: boolean) => void;
}
const UserModal = (props: Props) => {
  const { isOpen, setIsOpen } = props;
  const { user } = useContext(UserContext);
  return (
    <Modal visible={isOpen} onCancel={() => setIsOpen(false)} onOk={() => setIsOpen(false)}>
      <StyledDescription column={1} size="middle" title="Informações do usuario">
        <Descriptions.Item label="Nome">{ user.conta.nome }</Descriptions.Item>
        <Descriptions.Item label="Email">{user.conta.email}</Descriptions.Item>
        <Descriptions.Item label="Privilégios">{user.conta.privilegio }</Descriptions.Item>
        <Descriptions.Item label="Status">{ user.conta.ativo ? 'Ativo' : 'Pendente' }</Descriptions.Item>
        <Descriptions.Item label="Módulos">
          {user.conta.modulos.map((modulo, index) => (index === user.conta.modulos.length - 1 ? `${modulo}.` : `${modulo}, `))}
        </Descriptions.Item>
      </StyledDescription>
    </Modal>
  );
};

export default UserModal;

import React, { useContext } from 'react';
import { UserContext } from '../../context/UserContext/UserContext';
import useUserModal from '../../pages/dashboard/hooks/useUserModal';
import { StyledAvatar } from '../../styles/Avatar';
import { GhostButton } from '../../styles/GhostButton';
import { Logo } from '../../styles/Logo';
import UserModal from '../usermodal';
import { NavbarContainer } from './styles';

const Navbar = () => {
  const { user } = useContext(UserContext);
  const { setIsOpen, isOpen } = useUserModal();
  return (
    <NavbarContainer>
      <UserModal setIsOpen={setIsOpen} isOpen={isOpen} />
      <Logo width="100px" src="./icons/logo.svg" />
      <GhostButton type="button" onClick={() => setIsOpen(true)}>
        <StyledAvatar size={40} src={user.conta.urlImagemPerfil} />
      </GhostButton>
    </NavbarContainer>
  );
};

export default Navbar;

import styled from 'styled-components';

export const NavbarContainer = styled.nav`
    padding: 25px 35px;
    display: flex;
    justify-content: space-between;
    align-items: center;
`;

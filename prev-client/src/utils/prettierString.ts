function humanize(str: string) {
  let i; const
    frags = str.split('_');
  for (i = 0; i < frags.length; i += 1) {
    frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1);
  }
  return frags.join(' ');
}

export const prettierString = (dbString: string) => {
  const humanized = humanize(dbString);
  return humanized;
};

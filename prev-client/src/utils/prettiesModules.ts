import { ICounter } from '../context/DashboardContext/useDashboard';

// Foi necessário cria um switch caso, pois o typescript não aceitava a indexação direta
// ex: prettyModule[module.tipo]
export const prettierString = (module: ICounter) => {
  switch (true) {
    case module.tipo === 'CLIENTES':
      return 'Clientes';
    case module.tipo === 'PETICOES':
      return 'Petições';
    case module.tipo === 'CALCULOS':
      return 'Cálculos';
    case module.tipo === 'CASOS':
      return 'Casos';
    default:
      return '';
  }
};

export const prettierModule = (module: ICounter) => {
  switch (true) {
    case module.tipo === 'CLIENTES':
      return '#1B77C5';
    case module.tipo === 'PETICOES':
      return '#FF8853';
    case module.tipo === 'CALCULOS':
      return '#00A881';
    case module.tipo === 'CASOS':
      return '#6838B7';
    default:
      return '#1B77C5';
  }
};

export const prettierIcon = (module: ICounter) => {
  switch (true) {
    case module.tipo === 'CLIENTES':
      return './icons/userIcon.svg';
    case module.tipo === 'PETICOES':
      return './icons/peticoesIcon.svg';
    case module.tipo === 'CALCULOS':
      return './icons/calcIcon.svg';
    case module.tipo === 'CASOS':
      return './icons/casesIcon.svg';
    default:
      return './icons/userIcon.svg';
  }
};

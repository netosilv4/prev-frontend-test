import React, { useContext } from 'react';
import {
  BrowserRouter, Navigate, Route, Routes,
} from 'react-router-dom';
import { UserContext } from './context/UserContext/UserContext';
import Dashboard from './pages/dashboard';
import Login from './pages/login';

const App = () => {
  const { token } = useContext(UserContext);
  return (
    <BrowserRouter>
      <Routes>
        {
          token
            ? (
              <>
                <Route path="/" element={<Dashboard />} />
                <Route path="*" element={<Navigate to="/" />} />
              </>
            )
            : (
              <>
                <Route path="/login" element={<Login />} />
                <Route path="*" element={<Navigate to="/login" />} />
              </>
            )
        }
      </Routes>
    </BrowserRouter>
  );
};

export default App;

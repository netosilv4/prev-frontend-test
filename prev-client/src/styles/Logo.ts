import styled from 'styled-components';

export const Logo = styled.img`
    width: ${(props) => props.width || '100%'};
    height: ${(props) => props.height || 'auto'};
    :hover {
        cursor: pointer;
    }
`;

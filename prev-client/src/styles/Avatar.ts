import Avatar from 'antd/lib/avatar/avatar';
import styled from 'styled-components';

export const StyledAvatar = styled(Avatar)`
    :hover {
        cursor: pointer;
    }
`;

<div align="center"><h1>Gamecommerce</h1></div>

## Summario

- [Introdução](#introdução)
- [Instruções para inicializar o projeto](#instruções-para-inicializar-o-projeto)
- [Detalhes da aplicação](#detalhes-da-aplicação)
  - [Front-end](#front-end)

### Introdução

Projeto Previdenciaria Frontend Challenger

## Instruções para inicializar o projeto

- Certifique-se de que as seguintes portas estão liberadas na sua maquina : [3000. 8080].

- Para inicializar o cliente entre na pasta raiz e navegue até a pasta "prev-client" e execute o seguinte comando "npm install", após a instalação, execute "npm start" para inicializar o cliente.

- Para inicializar o servidor entre na pasta raiz e navegue até a pasta "prev-server" e execute o seguinte comando "npm install -g json-server", após a instalação, execute "json-server --watch db.json -p 8080 -m lmd.js --routes routes.json".

## Detalhes da aplicação

### Front-end

Algumas das tecnologias utilizadas para o desenvolvimento:

- React
- Typescript
- Styled-components
- axios
- sweetAlert2
- moment